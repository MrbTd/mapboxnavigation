import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import MapboxNavigation from '@homee/react-native-mapbox-navigation';

const App = () => {
  return (
    <View style={styles.container}>
      <Text style={{ textAlign: 'center', padding: "2%", color: "black", fontWeight: "bold", fontSize: 18 }}>TEST MATCAGO AKIL TECHNOLOGIE ❤️</Text>
      <MapboxNavigation
        origin={[-3.973666, 5.284626]}
        destination={[-3.938668, 5.370210]}
        shouldSimulateRoute
        showsEndOfRouteFeedback
        onLocationChange={(event) => {
          const { latitude, longitude } = event.nativeEvent;
        }}
        onRouteProgressChange={(event) => {
          const {
            distanceTraveled,
            durationRemaining,
            fractionTraveled,
            distanceRemaining,
          } = event.nativeEvent;
        }}
        onError={(event) => {
          const { message } = event.nativeEvent;
        }}
        onCancelNavigation={() => {
          // User tapped the "X" cancel button in the nav UI
          // or canceled via the OS system tray on android.
          // Do whatever you need to here.
        }}
        onArrive={() => {
          // Called when you arrive at the destination.
        }}
      />
    </View>
  )
}

export default App

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})